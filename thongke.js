$(document).ready(function(){
    //PIE CHART
    var donutData        = {
        labels: [
            'Hawaii',
            'Seafood',
            'Bacon',
            'Khác',
        ],
        datasets: [
            {
            data: [30,25,20,10],
            backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef'],
            }
        ],//đổi dữ liệu data của dataset
        changeData: function(){
            this.datasets.map((paramData)=>{
              return  paramData.data = [gNumberOfHawaii, gNumberOfSeafood,gNumberOfBacon,gNumberOfAnother]
            })
            
        }
    }
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = donutData
    var pieOptions     = {
    maintainAspectRatio : false,
    responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
    type: 'pie',
    data: pieData,
    options: pieOptions
    })


    //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var StatusData        = {
      labels: [
          'Open',
          'Confirmed',
          'Cancel',
          
      ],
      datasets: [
        {
          data: [700,300,100],
          backgroundColor : ['rgba(241, 129, 207, 0.603)', 'rgb(241, 129, 129)', 'rgba(238, 84, 174, 0.767)'],
        }
      ],
      changeDonutData:function(){
        this.datasets.map((paramData)=>{
            return   paramData.data = [gOpenNumber, gConfirmedNumber,gCancelNumber]
        })
        
      }
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: StatusData,
      options: donutOptions
    })
    
      /*
       * END DONUT CHART
       */
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gOrderDB = {
        orders :[],
        changeNullValue:function(){//chuyển các giá trị null trong obj thành rỗng trước khi lọc
            this.orders.forEach(function(paramOrder){
                  for(var key in paramOrder) {
                      if(paramOrder[key] == null){
                        paramOrder[key] = "";
                      }
                      
                  }
              });
        },
        filterSeafoodPizzaType:function(){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return paramOrder.loaiPizza.toLowerCase() == "seafood";
            })
            return vFilterResult
        },
        filterHawaiiPizzaType:function(){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return paramOrder.loaiPizza.toLowerCase() == "hawaii";
            })
            return vFilterResult
        },
        filterBaconPizzaType:function(){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return paramOrder.loaiPizza.toLowerCase() == "bacon";
            })
            return vFilterResult
        },//filter loại pizza khác với 3 loại kia
        filterAnotherPizzaType:function(){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return (
                    (paramOrder.loaiPizza.toLowerCase() != "bacon") &&
                    (paramOrder.loaiPizza.toLowerCase() != "hawaii")&&
                    (paramOrder.loaiPizza.toLowerCase() != "seafood")
                )
            })
            return vFilterResult
        },
        filterOpenStatus: function(){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return paramOrder.trangThai.toLowerCase() == "open";
            })
            return vFilterResult
        },
        filterConfirmStatus: function(){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return paramOrder.trangThai.toLowerCase() == "confirmed";
            })
            return vFilterResult
        },
        filterCancelStatus: function(){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return paramOrder.trangThai.toLowerCase() == "cancel";
            })
            return vFilterResult
        },
        filterVoucher: function(){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return paramOrder.idVourcher != "";
            })
            return vFilterResult
        },
        filterDrink: function(){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return paramOrder.idLoaiNuocUong == "";
            })
            return vFilterResult
        },


    }
    //các biến chứa loại pizza và tổng số loại pizza
    var gSeafoodPizza = [];
    var gHawaiPizza = [];
    var gBaconPizza = [];
    var gAnotherPizza = [];
    var gNumberOfHawaii = 0;
    var gNumberOfSeafood = 0;
    var gNumberOfBacon = 0;
    var gNumberOfAnother = 0;

    //các biến chứa loại status và tổng số status
    var gOpenStatus = [];
    var gConfirmedStatus = [];
    var gCancelStatus = [];



    var gOpenNumber = 0;
    var gConfirmedNumber = 0;
    var gCancelNumber = 0;

    //các biến chứa loại voucher và số lượng
    var gVoucher = [];
    var gVoucherNumber = 0;
    //các biến chứa loại nuowsc uong và số lượng
    var gDrink = [];
    var gDrinkNumber = 0;
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm xử lý sự kiện load trang
    function onPageLoading(){
        "use strict";
        
        //Load dữ liệu đơn hàng vào bảng
        callApiToGetAllOrder();
        
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    function callApiToGetAllOrder(){
        "use strict";
        $.ajax({
            type: "GET",
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            dataType: "json",
            success: function(paramResObj){
                gOrderDB.orders = paramResObj;
                $('#sale').html(paramResObj.length)
                //chuyen doi dữ liệu rỗng
                gOrderDB.changeNullValue();
                changePieChartValue();
                changeStatusChart();
                //lọc voucher
                gVoucher = gOrderDB.filterVoucher();
                gVoucherNumber = gVoucher.length
                $('#voucher').html(gVoucherNumber)
                //lọc người dùng không dùng đồ uông
                gDrink = gOrderDB.filterDrink();
                gVoucherNumber = gVoucher.length
                $('#drink').html(gDrink.length)
            },
            error: function(paramErr){
                
            }
        })
    }

    function changePieChartValue(){
        "use strict";
        //lọc dữ liệu pizza
        gHawaiPizza = gOrderDB.filterHawaiiPizzaType()          
        gSeafoodPizza = gOrderDB.filterSeafoodPizzaType();
        gBaconPizza = gOrderDB.filterBaconPizzaType();
        gAnotherPizza = gOrderDB.filterAnotherPizzaType();
        //lấy số lượng pizza
        gNumberOfHawaii = gHawaiPizza.length;
        gNumberOfSeafood = gSeafoodPizza.length;
        gNumberOfBacon = gBaconPizza.length;
        gNumberOfAnother = gAnotherPizza.length;
        donutData.changeData()
    }

    function changeStatusChart(){
        "use strict";
        //lọc dữ liệu trạng thái
        gOpenStatus = gOrderDB.filterOpenStatus();
        gConfirmedStatus = gOrderDB.filterConfirmStatus();
        gCancelStatus = gOrderDB.filterCancelStatus();
        //lấy số lượng
        gOpenNumber = gOpenStatus.length
        
        gConfirmedNumber = gConfirmedStatus.length
       
        gCancelNumber = gCancelStatus.length
        
        StatusData.changeDonutData();
      
    }
    
})