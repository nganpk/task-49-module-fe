$(document).ready(function(){
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    
    /** Các biến toàn cục hằng số Form mode: 4 trạng thái của form. Mặc định sẽ là Normal
   *
    ** Khi ấn vào nút Thêm, cần đổi biến trạng thái về trạng thái Insert
    ** Khi ấn vào nút Sửa, cần đổi biến trạng thái về trạng thái Update
    ** Khi ấn vào nút Xóa, cần đổi biến trạng thái về trạng thái Delete
    *
    * Tại một thời điểm, trạng thái của form luôn là 1 trong 4 trạng thái
    */ 
    var gFORM_MODE_NORMAL = "Normal";
    var gFORM_MODE_INSERT = "Insert";
    var gFORM_MODE_UPDATE = "Update";
    var gFORM_MODE_DELETE = "Delete";

    // biến toàn cục cho trạng thái của form: mặc định ban đầu là trạng thái Normal
    var gFormMode = gFORM_MODE_NORMAL;
    //Biến chứa dữ liệu đơn hàng lấy từ api
    var gOrderDB = {
        orders:[],
        changeNullValue:function(){//chuyển các giá trị null trong obj thành rỗng trước khi lọc
            this.orders.forEach(function(paramOrder){
                  for(var key in paramOrder) {
                      if(paramOrder[key] == null){
                        paramOrder[key] = "";
                      }
                      
                  }
              });
        },
        //phương thức lọc dữ liệu
        filterOrder: function(paramFilterObj){
            var vFilterResult = [];
            vFilterResult = this.orders.filter((paramOrder) => {
                return (
                    (paramOrder.trangThai == paramFilterObj.trangThai || paramFilterObj.trangThai === "")&&
                    (paramOrder.loaiPizza.toUpperCase() == paramFilterObj.loaiPizza.toUpperCase() || paramFilterObj.loaiPizza === "")
                )
            })
            return vFilterResult;
        },//phương thức thay thế khi sửa dữ liệu thành công trên api
        replaceObjAfterUpdated: function (paramResponseObj){
            this.orders.map((paramOrder,paramIndex)=>{
              if(paramOrder.id === paramResponseObj.id){
                return  this.orders.splice(paramIndex, 1, paramResponseObj);
              }
            })
        },//phương thức add order mới vào 
        addNewOrder: function(paramOrder){
          this.orders.push(paramOrder); 
        },
        
    }

    //tạo biến chứa id của đơn hàng
    var gId = [];
    //tạo biến chứa order id của đơn hàng
    var gOrderId = [];
    //Tạo biến chứa thuộc tính của bảng
    const gORDER_COLUMNS = ["orderId", "hoTen", "soDienThoai", "kichCo", "loaiPizza", "idLoaiNuocUong", "trangThai", "action"]
    //thứ tự các cột
    const gCOL_ORDER_ID = 0;
    const gCOL_HO_TEN = 1;
    const gCOL_DIEN_THOAI = 2;
    const gCOL_KICH_CO = 3;
    const gCOL_LOAI_PIZZA = 4;
    const gCOL_LOAI_NUOC = 5;
    const gCOL_TRANG_THAI = 6;
    const gCOL_ACTION = 7;

    //TẠO Bảng
    var gOrderTable = $('#order-table').DataTable({
        ordering:false,
        columns:[
            {data: gORDER_COLUMNS[gCOL_ORDER_ID]},
            {data: gORDER_COLUMNS[gCOL_HO_TEN]},
            {data: gORDER_COLUMNS[gCOL_DIEN_THOAI]},
            {data: gORDER_COLUMNS[gCOL_KICH_CO]},
            {data: gORDER_COLUMNS[gCOL_LOAI_PIZZA]},
            {data: gORDER_COLUMNS[gCOL_LOAI_NUOC]},
            {data: gORDER_COLUMNS[gCOL_TRANG_THAI]}, 
            {data: gORDER_COLUMNS[gCOL_ACTION]}
        ],
        columnDefs:[
            {//ĐỊNH NGHĨA LẠI CỘT ACTION
                targets: gCOL_ACTION,
                className: "align-middle",
                defaultContent: `
                    <button class="btn order-detail text-white  btn-sm" style="background-color: #6FB98F"><i class="fas fa-info-circle" style="font-size:10px;"></i> Edit</button>
                    <button class="btn delete-detail text-white btn-sm" style="background-color: #8EBA43"><i class="fas fa-trash" style="font-size:10px;"></i> Xoá</button>
                `
            },
            {
                targets:[gCOL_ORDER_ID, gCOL_HO_TEN,gCOL_DIEN_THOAI,gCOL_KICH_CO, gCOL_LOAI_PIZZA, gCOL_LOAI_NUOC,gCOL_TRANG_THAI],
                className: "align-middle"
            }
        ]
    });
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút lọc dữ liệu đơn hàng
    $('#btn-filter-data').on('click', onBtnFilterOrderClick);
    //gán sự kiện cho nút chi tiết
    $('#order-table').on('click', '.order-detail', function(){
        onBtnOrderDetailClick(this);
    })
    
    //reset dữ liệu khi click nút tắt modal
    $('#detail-modal').on('hidden.bs.modal', function () {
        
        resetForm();
    })
    //gán sự kiện cho nút xác nhận đơn hàng trong modal
    $('#btn-confirm-order').on('click', onBtnConfirmOrderClick);
    
    //gán sự kiện cho nút tạo thêm đơn hàng mới
    $('#btn-add-order').on('click', onBtnCreateNewOrderClick);
    
    //gán sự kiện cho nút xoá đơn hàng
    $('#order-table').on('click', '.delete-detail', function(){
        onBtnDeleteOrderClick(this);
    })
    //gán sự kiện cho nút xác nhận xoá đơn hàng
    $('#btn-confirm-delete').on('click', onBtnConfirmDeleteClick);
    //gán sư kiện cho nút huỷ đơn hàng
    $('#delete-modal').on('hidden.bs.modal', function () {
        resetForm();
    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm xử lý sự kiện load trang
    function onPageLoading(){
      "use strict";
      //Set trạng thái normal
      gFormMode = gFORM_MODE_NORMAL;
      $('#div-form-mod').html(gFormMode);
      //Load dữ liệu đơn hàng vào bảng
      callApiToLoadOrderToTable();
      //load dữ liệu đồ uống vào select modal
      callApiGetDrinkListLoadToSelect();
    }

    //Hàm xử lý sự kiện ấn nút lọc dữ liệu đơn hàng
    function onBtnFilterOrderClick(){
        "use strict";
        //tạo đối tượng
        var vFilterObj = {
            trangThai:"",
            loaiPizza: ""
        }
        var vFilterResult = [];
        //Thu thập dữ liệu
        vFilterObj.trangThai = $('#select-trangthai').val();
        vFilterObj.loaiPizza = $('#select-loaipizza').val();
        //Lọc dữ liệu
        vFilterResult = gOrderDB.filterOrder(vFilterObj);

        //load dữ liệu đã lọc vào bảng
        loadDataToTable(vFilterResult);
        
    }
    //Hàm xử lý sự kiện ấn nút chi tiết
    function onBtnOrderDetailClick(paramDetailButton){
        "use strict";

        //Set trạng thái update
        gFormMode = gFORM_MODE_UPDATE;
        $('#div-form-mod').html(gFormMode);

        //lấy dữ liệu của dòng
        var vRowSelected = $(paramDetailButton).parents('tr')
        var vDataTableRow = gOrderTable.row(vRowSelected);
        var vRowData = vDataTableRow.data();

        //Lấy id của dòng
        gId = vRowData.id
        //lấy order id của dòng
        gOrderId = vRowData.orderId
        //Hiện modal
        $('#detail-modal').modal('show')
        
        //Thêm input order id và trạng thái
        addMoreHtmlElement();
        //Thêm read only vào 
        addReadOnlyToForm();
        //Hiển thị dữ liệu lên modal
        showOrderDetailToModal(vRowData);
        //lấy chi tiết đơn hàng bằng order id của dòng
        callApiToGetOrderByOrderId();

        
    }

    //Hàm xử lý sự kiện ấn nút xác nhận đơn hàng trong modal
    function onBtnConfirmOrderClick(){
        "use strict";
        if (gFormMode === gFORM_MODE_INSERT) {//thêm mới
           //tạo đối tượng
            var vOrderRequest = {
                kichCo: "",
                duongKinh: "",
                suon: "",
                salad: "",
                loaiPizza: "",
                idVourcher: "",
                idLoaiNuocUong: "",
                soLuongNuoc: "",
                hoTen: "",
                thanhTien: "",
                email: "",
                soDienThoai: "",
                diaChi: "",
                loiNhan: ""
            }
            
            getOrderInfo(vOrderRequest);
            
            var vIsValidated = validateOrderInfo(vOrderRequest)
            if(vIsValidated){
                if(vOrderRequest.idVourcher === ""){
                    callApiCreateNewOrder(vOrderRequest);
                    
                } else {
                    callApiToCheckVoucherCode(vOrderRequest.idVourcher, vOrderRequest)
                }
                
            }
            
            
        } 
          else {//sửa
            //tạo đối tượng
            var vObjectRequest = {
                trangThai: "" 
            }
            //Thu thập dữ liệu
            vObjectRequest.trangThai = $('#select-status').val()
            //Validate dữ liệu
            if(vObjectRequest.trangThai === ""){
                var vModalWarning = $('#modal-warning');
                var vModalWarningBody = $('#modal-body');
                
                vModalWarning.modal('show');
                vModalWarningBody.html('Xin vui lòng chọn trạng tháu')
                    
                
            } else {
                //Gọi Api update đơn hàng
                callApiToUpdateOrderStatus(vObjectRequest);
                //báo cập nhật thành công
                alert('dữ liệu cập nhật thành công')
                //tắt modal sau khi update thành công
                $('#detail-modal').modal('hide');
                //reset form
                resetForm();
                //reset trạng thái normal
                gFormMode = gFORM_MODE_NORMAL;
                $('#div-form-mod').html(gFormMode);
            }
            
        }
        
    }
   

    
    //Hàm xử lý sự kiện ấn nút xoá đơn hàng
    function onBtnDeleteOrderClick(paramDeleteButton){
        "use strict";
        //Set trạng thái Delete
        gFormMode = gFORM_MODE_DELETE;
        $('#div-form-mod').html(gFormMode);
        
        //lấy dữ liệu của dòng
        var vRowSelected = $(paramDeleteButton).parents('tr')
        var vDataTableRow = gOrderTable.row(vRowSelected);
        var vRowData = vDataTableRow.data();

        //Lấy id của dòng
        gId = vRowData.id
        //lấy order id của dòng
        gOrderId = vRowData.orderId

        //Hiện modal
        $('#delete-modal').modal('show')
    }

    //Hàm xử lý sự kiện ấn nút xác nhận xoá đơn hàng
    function onBtnConfirmDeleteClick(){
        "use strict";
        callApiToDeleteOrder();
    }
    
    //Hàm xử lý sự kiện ấn nút save data
    function onBtnCreateNewOrderClick(){
        "use strict";
        //Set trạng thái insert
        gFormMode = gFORM_MODE_INSERT;
        $('#div-form-mod').html(gFormMode);
        //Hiển thị modal
        $('#detail-modal').modal('show');
        //chọn combo nào sẽ hiển thị thông tin combo đó lên input
        $('#select-kichco').on('change',function(){
            onSelectChangeValue(this)
        })
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý sự kiện gọi Api load dữ liệu đơn hàng vào bảng vào bảng
    function callApiToLoadOrderToTable(){
        "use strict";
        $.ajax({
            type: "GET",
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            dataType: "json",
            success: function(paramResObj){
                gOrderDB.orders = paramResObj;
                //chuyển đổi các thuộc tính null thành rỗng
                gOrderDB.changeNullValue();
                //load response trả về lên bảng
                loadDataToTable(gOrderDB.orders);
            },
            error: function(paramErr){
               
            }
        })
    }
    
    //Hàm xử lý load dữ liệu vào bảng
    function loadDataToTable(paramOrderArr){
        "use strict";
        gOrderTable.clear();
        gOrderTable.rows.add(paramOrderArr);
        gOrderTable.draw();
    }
    //Hàm gọi api lấy dữ liệu đồ uống load vào select modal
    function callApiGetDrinkListLoadToSelect(){
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: "GET",
            dataType: "json",
            success: function(paramResObj){
                loadDrinkListToSelect(paramResObj);
            }
        })
    }

    //hàm load dữ liệu đồ uống vào select modal
    //inp: paramDrinkArr: danh sách đồ uống lấy từ api về
    //out: load dữ liệu lên trên select loại đồ uống trên modal chi tiết đơn hàng
    function loadDrinkListToSelect(paramDrinkArr){
        "use strict";
        var vSelectDrink = $('#select-loainuocuong');
        for(var bI = 0; bI < paramDrinkArr.length; bI ++){
            vSelectDrink.append($('<option>', {
                value: paramDrinkArr[bI].maNuocUong,
                text: paramDrinkArr[bI].tenNuocUong
            }));
        }
    }
    //Hàm load dữ liệu đơn hàng đã update vào bảng
    //truyền dữ liệu false để khi update trạng thái đơn hàng sẽ update tại vị trí trang hiện tại mà không cần F5 lại trang
    function loadUpdatedOrderToTable(paramOrderArr){
        "use strict";
        gOrderTable.clear();
        gOrderTable.rows.add(paramOrderArr);
        gOrderTable.draw(false);
    }
    //Hàm thêm html vào modal
    function addMoreHtmlElement(){
        "use strict";
        //Thêm html order id
        $('#div-content').show()
        $('#div-content').html("")
        var vDivContentOrderId = $('#div-content')
        var vOrderIdHtml = `
        <label class="col-sm-3 col-form-label">Order Id</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="input-orderid" placeholder="Order Id" readonly>
        </div> 
        `
        vDivContentOrderId.append(vOrderIdHtml)
        //Thêm html trạng thái
        $('#div-content-2').show()
        $('#div-content-2').html("")
        var vDivContentStatus = $('#div-content-2')
        var vStatusHtml = `
        <label class="col-sm-3 col-form-label">Trạng thái</label>
        <div class="col-sm-9">
            <select id="select-status" class="form-control " style="width: 100%;">
                <option selected="selected" value="" disabled>Chọn trạng thái</option>
                <option value="open">Open</option>
                <option value="confirmed">Confirmed</option>
                <option value="cancel">Cancel</option>
                <option id="option-status" style="display:none;"></option>
                                           
            </select>  
        </div> 
        `
        vDivContentStatus.append(vStatusHtml)
       
        //thêm option cho size
        
        var vSelectSize = $('#select-kichco')
        var vSelectSizeHtml = `<option id="option-size"></option>`
        vSelectSize.append(vSelectSizeHtml)

        //thêm option cho pizza
        var vSelectPizza = $('#select-modal-loaipizza')
        var vSelectPizzaHtml = `<option id="option-pizza"></option>`
        vSelectPizza.append(vSelectPizzaHtml)

        //thêm option cho đồ uống
        var vSelectDrink = $('#select-loainuocuong');
        var vSelectDrinkHtml = `<option id="option-drink"></option>`
        vSelectDrink.append(vSelectDrinkHtml)

    }

    //Hàm show dữ liệu đơn hàng lên lên modal
    //inp: paramOrderDetail: thông tin gắn liền với nút chi tiết
    //out: thể hiện các thông tin đó lên modal chi tiêt đơn hàng
    function showOrderDetailToModal(paramOrderDetail){
        "use strict";
        //Hiển thị thông tin order id
        $('#input-orderid').val(paramOrderDetail.orderId);

        //Hiển thị thông tin kích cỡ combo lên modal
        var vSelectCombo = $('#select-kichco').val()
        if(paramOrderDetail.kichCo == vSelectCombo){//thông tin trùng với value option có sẵn trong select
            $('#select-kichco').val(paramOrderDetail.kichCo);
            $('#select-kichco').select2({theme: 'bootstrap4'}).trigger('change')
        } else {//thông tin khác với value có sẵn trong option
            var vOptionSizeCombo = $('#option-size')
            vOptionSizeCombo.val(paramOrderDetail.kichCo);
            vOptionSizeCombo.text(paramOrderDetail.kichCo);
            var vOptionComboValue = $('#option-size').val()
            $('#select-kichco').val(vOptionComboValue);
            $('#select-kichco').select2({theme: 'bootstrap4'}).trigger('change')
        }
        //thông tin chi tiết của các combo được chọn
        $('#input-duongkinh').val(paramOrderDetail.duongKinh);
        $('#input-suonnuong').val(paramOrderDetail.suon);
        $('#input-salad').val(paramOrderDetail.salad);
        $('#input-soluongnuoc').val(paramOrderDetail.soLuongNuoc);
        $('#input-thanhtien').val(numberWithCommas(paramOrderDetail.thanhTien));

        //Hiển thị thông tin loại pizza lên modal
        var vSelectPizza = $('#select-modal-loaipizza').val()
        if(paramOrderDetail.loaiPizza == vSelectPizza){
            $('#select-modal-loaipizza').val(paramOrderDetail.loaiPizza);
            $('#select-modal-loaipizza').select2({theme: 'bootstrap4'}).trigger('change')
        } else { //trong trương hợp value khác với 3 giá trị set cho select lúc ban đầu
            var vOptionPizza = $('#option-pizza')
            vOptionPizza.val(paramOrderDetail.loaiPizza);
            vOptionPizza.text(paramOrderDetail.loaiPizza);
            var vOptionPizzaValue = $('#option-pizza').val()
            $('#select-modal-loaipizza').val(vOptionPizzaValue);
            $('#select-modal-loaipizza').select2({theme: 'bootstrap4'}).trigger('change')
        }

        //Thông tin mã voucher
        $('#input-mavoucher').val(paramOrderDetail.idVourcher);

        //Hiển thị thông tin loại nước uống lên modal
        var vSelectDrink = $('#select-loainuocuong').val()
        if(paramOrderDetail.idLoaiNuocUong == vSelectDrink){//thông tin trường với value option ban đầu
            $('#select-loainuocuong').val(paramOrderDetail.idLoaiNuocUong);
            $('#select-loainuocuong').select2({theme: 'bootstrap4'}).trigger('change')
        } else {//thông tin khác với value option ban đầu
            var vOptionDrink = $('#option-drink')
            vOptionDrink.val(paramOrderDetail.idLoaiNuocUong);
            vOptionDrink.text(paramOrderDetail.idLoaiNuocUong);
            var vOptionDrinkValue = $('#option-drink').val()
            $('#select-loainuocuong').val(vOptionDrinkValue);
            $('#select-loainuocuong').select2({theme: 'bootstrap4'}).trigger('change')
        }
        
        //thông tin của khách hàng
        $('#input-hovaten').val(paramOrderDetail.hoTen);
        $('#input-sodienthoai').val(paramOrderDetail.soDienThoai);
        $('#input-email').val(paramOrderDetail.email);
        $('#input-diachi').val(paramOrderDetail.diaChi);
        //thông tin lời nhắn
        $('#input-loinhan').val(paramOrderDetail.loiNhan);

        //Hiển thị thông tin trạng thái lên modal
        
        var vSelectStatus = $('#select-status').val()
        if(paramOrderDetail.trangThai == vSelectStatus){//thông tin trường với value option ban đầu
            $('#select-status').val(paramOrderDetail.trangThai);
        } else {//thông tin khác với value option ban đầu
            var vOptionStatus = $('#option-status')
            vOptionStatus.val(paramOrderDetail.trangThai);
            vOptionStatus.text(paramOrderDetail.trangThai);
            var vOptionStatusValue = $('#option-status').val()
            $('#select-status').val(vOptionStatusValue);
            
        }

        //thông tin số tiền phải thanh toán
        $('#input-thanhtoan').val(numberWithCommas(paramOrderDetail.thanhTien - paramOrderDetail.giamGia)); 
    }

    //Hàm thêm read only khi bật nút chi tiết
    function addReadOnlyToForm(){
        "use strict";
        $('#select-kichco').attr("disabled",true);
        $('#select-modal-loaipizza').attr("disabled",true);
        $('#input-mavoucher').prop("readonly",true);
        $('#select-loainuocuong').attr("disabled",true);
        $('#input-hovaten').prop("readonly",true);
        $('#input-sodienthoai').prop("readonly",true);
        $('#input-email').prop("readonly",true);
        $('#input-diachi').prop("readonly",true);
        $('#input-loinhan').prop("readonly",true);
    }

    //Hàm gọi api lấy đơn hàng bằng order id
    function callApiToGetOrderByOrderId(){
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gOrderId,
            type: "GET",
            dataType: "json",
            success: function(paramResObj){

            },
            error: function(paramErr){
               
            }
        })
    }

    //Hàm gọi api update dữ liệu trạng thái đơn hàng
    //inp: paramObj: đối tượng muốn thay đổi trạng thái
    //out: trạng thái thay đổi được up lên api và load lại dữ liệu trên bảng
    function callApiToUpdateOrderStatus(paramObj){
        "use strict";
        $.ajax({
            type: "PUT",
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
            contentType:"application/json;charset=UTF-8",
            data: JSON.stringify(paramObj), 
            success: function(paramResObj){
               //thay thế responseText vào gOrderDB
               gOrderDB.replaceObjAfterUpdated(paramResObj);
               //load dữ liệu đã update lên bảng
               loadUpdatedOrderToTable(gOrderDB.orders)
            },
            error: function(paramErr){
               
            }
        })
    }


    //Hàm xử lý thu thập dữ liệu cho đơn hàng muốn tạo
    //inp: paramOrder(đối tượng muốn tạo)
    //out: thu thập thông tin thông qua các trường dữ liệu trên web cho đối tượng
    function getOrderInfo(paramOrder){
        "use strict";
        paramOrder.kichCo = $('#select-kichco').val();
        paramOrder.duongKinh = $('#input-duongkinh').val();
        paramOrder.suon =  $('#input-suonnuong').val();
        paramOrder.salad = $('#input-salad').val();
        paramOrder.soLuongNuoc = $('#input-soluongnuoc').val();
        paramOrder.thanhTien = $('#input-thanhtien').val();
        paramOrder.loaiPizza = $('#select-modal-loaipizza').val();
        paramOrder.idVourcher = $('#input-mavoucher').val();
        paramOrder.idLoaiNuocUong = $('#select-loainuocuong').val();
        paramOrder.hoTen = $('#input-hovaten').val().trim();
        paramOrder.email =$('#input-email').val().trim();
        paramOrder.soDienThoai = $('#input-sodienthoai').val().trim();
        paramOrder.diaChi = $('#input-diachi').val().trim();
        paramOrder.loiNhan = $('#input-loinhan').val().trim();
    }

    //Hàm xử lý lấy dữ liệu đường kính, salad... khi select thay đổi
    function onSelectChangeValue(){
        "use strict";
        var vCombo = $('#select-kichco').val()
        
        if(vCombo == "S"){
            $('#input-duongkinh').val("20");
            $('#input-suonnuong').val("2");
            $('#input-salad').val("200");
            $('#input-soluongnuoc').val("2");
            $('#input-thanhtien').val("150000");
           $('#input-thanhtoan').val(numberWithCommas(150000))
        }
        if(vCombo == "M"){
            $('#input-duongkinh').val("25");
            $('#input-suonnuong').val("4");
            $('#input-salad').val("300");
            $('#input-soluongnuoc').val("3");
            $('#input-thanhtien').val("200000");
            $('#input-thanhtoan').val(numberWithCommas(200000))
        }
        if(vCombo == "L"){
            $('#input-duongkinh').val("30");
            $('#input-suonnuong').val("8");
            $('#input-salad').val("500");
            $('#input-soluongnuoc').val("4");
            $('#input-thanhtien').val("250000");
            $('#input-thanhtoan').val(numberWithCommas(250000))
        }
    }

    //Hàm validate dữ liệu đơn hàng muốn tạo
    function validateOrderInfo(paramOrderInfo){
        "use strict";
        var vModalWarning = $('#modal-warning');
        var vModalWarningBody = $('#modal-body');
        //validate họ tên
        if(paramOrderInfo.hoTen === ""){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng nhập họ và tên')
            return false;
        }
        //validate số điện thoại
        var vValidPhoneNumber = validatePhoneNumber(paramOrderInfo.soDienThoai)
        if(!vValidPhoneNumber){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng nhập số điện thoại')
            return false;
        }

        //validate địa chỉ
        if(paramOrderInfo.diaChi === ""){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng nhập địa chỉ')
            return false;
        }
        //validate kích cỡ combo
        if(paramOrderInfo.kichCo == null){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng chọn kích cỡ')
            return false;
        }
        //validate loại pizza
        if(paramOrderInfo.loaiPizza == null){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng chọn loại pizza')
            return false;
        }
        //validate loại nước uống
        if(paramOrderInfo.idLoaiNuocUong == null){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng chọn loại nước uống')
            return false;
        }
        
        //validate mã voucher
        if(isNaN(paramOrderInfo.idVourcher)){
            vModalWarning.modal('show');
            vModalWarningBody.html('Mã voucher không hợp lệ');
            //mã voucher không hợp lệ sẽ trả về voucher rỗng
            paramOrderInfo.idVourcher = "";
            $('#show-voucher').show().html('Voucher không hợp lệ')
            var vConverMoney =  numberWithCommas(paramOrderInfo.thanhTien)
            $('#input-thanhtoan').val(vConverMoney)
            return false;
        }
        
        var vValidEmail = validateEmail(paramOrderInfo.email)
        if(!vValidEmail){
            vModalWarning.modal('show');
            vModalWarningBody.html('Email không hợp lệ')
            return false;
        }

        return true;
    }
    
    //hàm validate số điện thoại
    function validatePhoneNumber(paramPhoneNum){
        "use strict";
        var vFormat = /^0([1-2]\d{9}|[1-9]\d{8})$/;
        if(! vFormat.test(paramPhoneNum)){
            return false;
        }
        return true;
    }

    //Hàm validate email
    function validateEmail(paramEmail) {
        "use strict";
        var vFormat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(paramEmail === ""){
            return true
        }
        if(paramEmail != ""){
            if ( ! vFormat.test(paramEmail) ) {
                return false;
            }
            return true;
        }
        
    }

    //Hàm gọi Api để check mã voucher
    function callApiToCheckVoucherCode(paramvoucher, paramObj){
        "use strict";
        $.ajax({
            type: "GET",
            url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + paramvoucher,
            dataType: "json",
            success: function(paramResObj){               
                handleVoucherCode(paramResObj);
                //gửi đơn hàng lên api
                callApiCreateNewOrder(paramObj);
                  
            },
            error: function(paramErr){
                var vPrice = parseInt($('#input-thanhtien').val())
                //thông báo không tìm thấy mã voucher
                var vModalWarning = $('#modal-warning');
                var vModalWarningBody = $('#modal-body');
                vModalWarning.modal('show');
                vModalWarningBody.html('Mã voucher không tồn tại');
                //ẩn vùng giảm giá
                $('#div-show-giamgia').hide()
                //ghi ra số tiền phải thanh toán
                $('#input-thanhtoan').val(numberWithCommas(vPrice))
            }
        })
    }
    //Hàm xử lý sự kiện sau khi kiểm tra voucher thành công
    //inp: paramResObj: voucher đúng được api trả về
    //out: 
    function handleVoucherCode(paramResObj){
        "use strict";
        //Hiển thị các vùng giảm giá
        $('#div-show-giamgia').show();
        //Tính tiền phải thanh toán và tổng số tiền được giảm
        var vPizzaPrice = parseInt($('#input-thanhtien').val())//đơn giá của pizza
        //số tiền phải thanh toán
        var vTotalPrice = PriceAfterDiscount(paramResObj.phanTramGiamGia, vPizzaPrice)
        var vConvertTotalPrice =  numberWithCommas(vTotalPrice)//quy đổi ra có dấu phẩy hàng ngàn
        //số tiền được giảm
        var vDiscountAmount = discountAmount(vPizzaPrice, vTotalPrice)
        var vConvertDiscountAmount = numberWithCommas(vDiscountAmount)//quy đổi ra dấu phẩy hàng ngàn
        //Hiển thị lên các trường input trên web
        $('#input-thanhtoan').val(vConvertTotalPrice)
        $('#input-giamgia').val(vConvertDiscountAmount)
    }
    //Hàm thêm dấu phẩy vào số tiền hàng ngàn
    function numberWithCommas(number) {
        "use strict";
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    //Hàm xử lý tính tiền sau khi trừ discount
    //inp: paramDiscount (phần trăm giảm giá), paramPrice(đơn giá pizza)
    //out: số tiền phải thanh toán sau khi giảm giá
    function PriceAfterDiscount(paramDiscount,paramPrice){
        "use strict";
        return paramPrice - (paramPrice*(paramDiscount/100));
    }

    //Hàm xử lý số tiền được giảm giá
    //inp: paramPizzaPrice (giá của pizza), paramFinalPrice(giá sau khi được giảm)
    //out: số tiền được giảm
    function discountAmount(paramPizzaPrice, paramFinalPrice){
        "use strict"; 
        return paramPizzaPrice - paramFinalPrice
    }

    //Hàm gọi api để tạo order mới
    //inp: paramOrder(đối tượng tạo trong nút xác nhận)
    //out: tải thông tin đơn hàng đã tạo lên api và load lại lên bảng
    function callApiCreateNewOrder(paramOrder){
        "use strict";
        $.ajax({
            type: "POST",
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders" ,
            contentType:"application/json;charset=UTF-8",
            data: JSON.stringify(paramOrder), 
            success: function(paramResObj){
                //báo cập nhật thành công
                alert('dữ liệu cập nhật thành công')
                //Tắt modal
                $('#detail-modal').modal('hide');
                //reset trạng thái normal
                gFormMode = gFORM_MODE_NORMAL;
                $('#div-form-mod').html(gFormMode);
                //reset lại các trường input trong modal
                resetForm();    
                $('#order-modal').modal('show');
                $('#inp-madonhang').val(paramResObj.orderId)
               //Thêm order mới vào danh sách
               gOrderDB.addNewOrder(paramResObj);
               //load dữ liệu đã update lên bảng
               loadDataToTable(gOrderDB.orders)
                $('#order-modal').on('hidden.bs.modal', function () {
                    location.reload()
                })
               
            },
            error: function(paramErr){
                
            }
        })
    }
    //Hàm gọi api để xoá dữ liệu đơn hàng
    function callApiToDeleteOrder(){
        "use strict";
        $.ajax({
            type: "DELETE",
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
            dataType: 'json', 
            success: function(paramResObj){
                //thông báo cập nhật thành công
                alert('Đã xoá thành công đơn hàng');
                //Tắt modal
                $('#delete-modal').modal('hide');
                //Reset form
                resetForm();
                //Load lại bảng
                location.reload()
            },
            error: function(paramErr){ 
            }
        })
    }
    //Hàm reset các trường dữ liệu

    function resetForm(){
        //reset trạng thái normal
        gFormMode = gFORM_MODE_NORMAL;
        $('#div-form-mod').html(gFormMode);
        "use strict";
        //reset thông tin khách hàng
        $('#input-hovaten').val("");
        $('#input-sodienthoai').val("");
        $('#input-email').val("");
        $('#input-diachi').val("");

        //reset thông tin đơn hàng
        //ẩn vùng chứa orderid
        $('#div-content').hide();
        //reset dữ liệu cho select combo và xoá đi option thêm vào
        $('#select-kichco').val("").trigger('change');
        $('#option-size').remove();
        //reset dữ liệu đi cùng với combo
        $('#input-duongkinh').val("");
        $('#input-suonnuong').val("");
        $('#input-salad').val("");
        $('#input-soluongnuoc').val("");
        $('#input-thanhtien').val("");
        //reset dữ liệu cho select loại pizza và xoá đi option thêm vào
        $('#select-modal-loaipizza').val("").trigger('change');
        $('#option-pizza').remove();
        //reset dữ liệu cho select loại nước uống và xoá đi option thêm vào
        $('#select-loainuocuong').val("").trigger('change');
        $('#option-drink').remove();
        //reset mã voucher
        $('#input-mavoucher').val("");
        //reset lại lời nhắn
        $('#input-loinhan').val("");
        //ẩn đi vùng hiển thị trạng thái
        $('#div-content-2').hide();
        //reset lại phải thanh toán
        $('#input-thanhtoan').val("");
        
        //Hiện lại các nút xoá và vùng check voucher
        
        $('#div-show-giamgia').hide(); 
        
        //bỏ trạng thái readonly
        $('#select-kichco').attr("disabled",false);
        $('#select-modal-loaipizza').attr("disabled",false);
        $('#input-mavoucher').prop("readonly",false);
        $('#select-loainuocuong').attr("disabled",false);
        $('#input-hovaten').prop("readonly",false);
        $('#input-sodienthoai').prop("readonly",false);
        $('#input-email').prop("readonly",false);
        $('#input-diachi').prop("readonly",false);
        $('#input-loinhan').prop("readonly",false);

    }

    
})